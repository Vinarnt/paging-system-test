package com.omega.paging.test;

import com.jme3.system.AppSettings;
import com.omega.paging.test.appstate.GameCore;

/**
 * test
 *
 * @author Vinarnt
 */
public class Main {

    public static void main(String[] args) {
        GameCore app = new GameCore();
        AppSettings settings = new AppSettings(true);
        settings.setResolution(800, 600);
        app.setSettings(settings);
        app.setShowSettings(false);
        app.start();
    }
}
