package com.omega.paging.test.appstate;

import com.jme3.app.SimpleApplication;
import com.jme3.renderer.RenderManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyu
 */
public class GameCore extends SimpleApplication {

    @Override
    public void simpleInitApp() {
        Logger.getLogger("").setLevel(Level.ALL);
        stateManager.attach(new PagingAppState());
    }

    @Override
    public void simpleUpdate(float tpf) {
        stateManager.update(tpf);
    }

    @Override
    public void simpleRender(RenderManager rm) {
        stateManager.render(rm);
    }
}
