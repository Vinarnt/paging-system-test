package com.omega.paging.test.appstate;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.omega.paging.PagingHandler;
import com.omega.paging.PagingManager;
import com.omega.paging.entity.PagingViewer;
import com.omega.paging.util.Vector3f;
import com.jme3.scene.debug.WireBox;
import com.jme3.scene.debug.WireSphere;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.omega.paging.PagingPatch;
import com.omega.paging.listener.PagedObjectListener;
import com.omega.paging.listener.PagingDebugListener;
import com.omega.paging.listener.PatchListener;
import com.omega.paging.test.paging.PagedCharacter;
import com.omega.paging.util.Vector3;
import java.util.logging.Logger;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Screen;

/**
 *
 * @author Kyu
 */
public class PagingAppState extends AbstractAppState {

    private static final Logger LOGGER = Logger.getLogger(PagingAppState.class.getName());
    private SimpleApplication app;
    private PagingManager pagingManager;
    private Node viewerNode;
    private Node viewerNode2;
    private Geometry viewerGeom;
    private Geometry viewerGeom2;
    private Geometry viewerRadius;
    private Screen guiScreen;
    private Label patchLabel;

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        this.app = (SimpleApplication) app;

        this.app.getFlyByCamera().setMoveSpeed(200f);

        guiScreen = new Screen(app);
        this.app.getGuiNode().addControl(guiScreen);

        Window win = new Window(guiScreen, "debugWin", new Vector2f(15, 15), new Vector2f(200, 100));
        win.setWindowTitle("Debug");
        patchLabel = new Label(guiScreen, new Vector2f(5f, 25f), new Vector2f(200f, 25f));
        win.addChild(patchLabel);
        guiScreen.addElement(win);

        pagingManager = PagingManager.getInstance();
        viewerNode = new Node("viewerNode");
        viewerNode2 = new Node("viewerNode2");
        viewerGeom = new Geometry("viewer", new Sphere(5, 5, 1));
        viewerGeom.setMaterial(new Material(this.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"));
        viewerGeom2 = new Geometry("viewer", new Sphere(5, 5, 1));
        viewerGeom2.setMaterial(new Material(this.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"));
        viewerNode.attachChild(viewerGeom);
        viewerNode2.attachChild(viewerGeom2);
        viewerRadius = new Geometry("viewerRadius", new WireSphere(50));
        viewerRadius.setMaterial(new Material(this.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"));
        viewerNode.attachChild(viewerRadius);
        this.app.getRootNode().attachChild(viewerNode);
        this.app.getRootNode().attachChild(viewerNode2);
        viewerNode2.setLocalTranslation(90, 0, 0);
        pagingManager.addViewer(new PagingViewer<Spatial>(viewerNode2) {
            @Override
            public Vector3f getPosition() {
                float[] loc = null;
                loc = object.getLocalTranslation().toArray(loc);
                tempVect.setArray(loc);

                return tempVect;
            }
        });
        pagingManager.addViewer(new PagingViewer<Spatial>(viewerNode) {
            @Override
            public Vector3f getPosition() {
                float[] loc = null;
                loc = object.getLocalTranslation().toArray(loc);
                tempVect.setArray(loc);

                return tempVect;
            }
        });

        PagingHandler pagedCharHandler = pagingManager.addHandler(PagedCharacter.class, new PagingHandler<>(50));
        //pagingManager.addHandler(PagedTerrainQuad.class, new PagingHandler<>(200));
        //pagingManager.addHandler(PagedBuilding.class, new PagingHandler<>(200));

        /*pagingManager.getHandler(PagedBuilding.class).addPaggedObject(new PagedBuilding());
         pagingManager.getHandler(PagedBuilding.class).addPaggedObject(new PagedBuilding());*/
        pagedCharHandler.addListener(new PagedObjectListener<PagedCharacter>() {
            @Override
            public void onInitialized(final PagedCharacter object) {
                //logger.log(Level.INFO, "Init paged object {0}", object);
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().attachChild(object.getObject());

                    return null;
                });
            }

            @Override
            public void onAdded(final PagedCharacter object) {
                //System.out.println("Add character");
                PagingAppState.this.app.enqueue(() -> {
                    //PagingAppState.this.app.getRootNode().attachChild(object.getObject());
                    ((Geometry) object.getObject()).getMaterial().setColor("Color", ColorRGBA.Blue);
                    ((Geometry) object.getObject()).move(0f, 10f, 0f);

                    return null;
                });

                //object.getObject().setCullHint(Spatial.CullHint.Inherit);
                /*PagingAppState.this.app.enqueue(new Callable<Void>() {
                 public Void call() throws Exception {
                 PagingAppState.this.app.getRootNode().attachChild(object.getObject());

                 return null;
                 }
                 });*/
            }

            @Override
            public void onRemoved(final PagedCharacter object) {
                //System.out.println("Remove character");
                PagingAppState.this.app.enqueue(() -> {
                    //((Geometry) object.getObject()).removeFromParent();
                    ((Geometry) object.getObject()).getMaterial().setColor("Color", ColorRGBA.Red);
                    ((Geometry) object.getObject()).move(0f, -10f, 0f);

                    return null;
                });

                //object.getObject().setCullHint(Spatial.CullHint.Always);
                /*PagingAppState.this.app.enqueue(new Callable<Void>() {
                 public Void call() throws Exception {
                 PagingAppState.this.app.getRootNode().detachChild(object.getObject());

                 return null;
                 }
                 });*/
            }

            @Override
            public void onDestroyed(final PagedCharacter object) {
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().detachChild(object.getObject());

                    return null;
                });
            }
        });
        PagingManager.getInstance().getHandler(PagedCharacter.class).addListener(new PatchListener() {
            @Override
            public void onInitialized(final PagingPatch patch) {
                //logger.log(Level.INFO, "Init patch {0}", patch);
                Vector3 patchLocation = patch.getLocation();
                float viewDistance = patch.getParent().getViewDistance();
                Geometry debugBox = createWireBox(new com.jme3.math.Vector3f(patchLocation.getX(), patchLocation.getY(), patchLocation.getZ()),
                        viewDistance, ColorRGBA.Orange);
                patch.setUserData(debugBox);
                
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().attachChild((Geometry) patch.getUserData());

                    return null;
                });
            }

            @Override
            public void onAdded(final PagingPatch patch) {
                ((Geometry) patch.getUserData()).getMaterial().setColor("Color", ColorRGBA.Green);
                
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().attachChild((Geometry) patch.getUserData());

                    return null;
                });
            }

            @Override
            public void onRemoved(final PagingPatch patch) {
                ((Geometry) patch.getUserData()).getMaterial().setColor("Color", ColorRGBA.Red);
                
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().detachChild((Geometry) patch.getUserData());

                    return null;
                });
            }

            @Override
            public void onDestroyed(final PagingPatch patch) {
                PagingAppState.this.app.enqueue(() -> {
                    PagingAppState.this.app.getRootNode().detachChild((Geometry) patch.getUserData());

                    return null;
                });
            }
        });
        pagingManager.getHandler(PagedCharacter.class).addListener((PagingDebugListener) (final PagingPatch patch) -> {
            PagingAppState.this.app.enqueue(() -> {
                patchLabel.setText("Patch : " + patch.getLocation());

                return null;
            });
        });
        /*pagingManager.getHandler(PagedTerrainQuad.class).addListener(new PagingListener<PagedTerrainQuad>() {
         public void onAdded(PagedTerrainQuad object) {
         System.out.println("Add terrain quad");
         object.getObject().setCullHint(Spatial.CullHint.Inherit);
         }

         public void onRemoved(PagedTerrainQuad object) {
         System.out.println("Remove terrain quad");
         object.getObject().setCullHint(Spatial.CullHint.Always);
         }
         });*/

        Geometry origin = new Geometry("origin", new Box(1, 1, 1));
        origin.setMaterial(new Material(this.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md"));
        origin.getMaterial().setColor("Color", ColorRGBA.Yellow);
        this.app.getRootNode().attachChild(origin);

        //pagingManager.getHandler(PagedCharacter.class).addPaggedObject(new PagedCharacter(this.app.getRootNode().getChild("sinbad")));
        for (int x = -50; x < 50; x++) {
            for (int z = -50; z < 50; z++) {
                Geometry testBox = new Geometry("", new Box(1f, 1f, 1f));
                Material newMat = new Material(this.app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
                testBox.setMaterial(newMat);
                testBox.setLocalTranslation(x * 10f, 10f, z * 10f);
                pagingManager.getHandler(PagedCharacter.class).addPaggedObject(new PagedCharacter(testBox));
            }

        }

        /*pagingManager.getHandler(PagedTerrainQuad.class).addPaggedObject(new PagedTerrainQuad((TerrainQuad) this.app.getRootNode().getChild("terrain-scene1Quad1")));
         pagingManager.getHandler(PagedTerrainQuad.class).addPaggedObject(new PagedTerrainQuad((TerrainQuad) this.app.getRootNode().getChild("terrain-scene1Quad2")));
         pagingManager.getHandler(PagedTerrainQuad.class).addPaggedObject(new PagedTerrainQuad((TerrainQuad) this.app.getRootNode().getChild("terrain-scene1Quad3")));
         pagingManager.getHandler(PagedTerrainQuad.class).addPaggedObject(new PagedTerrainQuad((TerrainQuad) this.app.getRootNode().getChild("terrain-scene1Quad4")));
         */
        
        // Set camera start position
        Camera cam = app.getCamera();
        cam.setLocation(new com.jme3.math.Vector3f(0f, 400f, 0f));
        cam.lookAt(viewerGeom.getWorldTranslation(), com.jme3.math.Vector3f.UNIT_Y);
        
        initialized = true;
    }

    @Override
    public void update(float tpf) {
        float centerX = 0f;
        float centerY = 0f;
        float radius = 150f;
        float speed = 10f;
        double speedScale = (0.001 * 2 * Math.PI) / speed;
        double angle = (this.app.getTimer().getTimeInSeconds() * 1000) * speedScale;

        float xCoord = (float) (centerX + Math.sin(angle) * radius);
        float zCoord = (float) (centerY + Math.cos(angle) * radius);

        viewerNode.setLocalTranslation(xCoord, 0f, zCoord);
        pagingManager.update();
    }

    @Override
    public void cleanup() {
        pagingManager.cleanup();

        initialized = false;
    }

    public PagingManager getPaggingManager() {
        return pagingManager;
    }

    private Geometry createWireBox(com.jme3.math.Vector3f pos, float size, ColorRGBA color) {
        pos.multLocal(size).subtractLocal(size / 2f, size / 2f, size / 2f);
        Geometry box = new Geometry("debugBox", new WireBox(size / 2f, size / 2f, size / 2f));
        Material mat = new Material(app.getAssetManager(), "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", color);
        box.setMaterial(mat);
        box.setLocalTranslation(pos);

        return box;
    }
}
