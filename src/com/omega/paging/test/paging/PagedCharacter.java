package com.omega.paging.test.paging;

import com.omega.paging.entity.PagedObject;
import com.omega.paging.util.Vector3f;
import com.jme3.scene.Spatial;

/**
 *
 * @author Kyu
 */
public class PagedCharacter extends PagedObject<Spatial> {

    public PagedCharacter(Spatial object) {
        super(object);
    }

    @Override
    public Vector3f getPosition() {
        float[] loc = null;
        loc = object.getLocalTranslation().toArray(loc);
        tempVect.setArray(loc);

        return tempVect;
    }
}
