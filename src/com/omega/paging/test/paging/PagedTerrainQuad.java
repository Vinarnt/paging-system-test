package com.omega.paging.test.paging;

import com.omega.paging.entity.PagedObject;
import com.omega.paging.util.Vector3f;
import com.jme3.terrain.geomipmap.TerrainQuad;

/**
 *
 * @author Kyu
 */
public class PagedTerrainQuad extends PagedObject<TerrainQuad> {

    public PagedTerrainQuad(TerrainQuad object) {
        super(object);
    }

    @Override
    public Vector3f getPosition() {
        float[] loc = null;
        loc = object.getWorldTranslation().toArray(loc);
        tempVect.setArray(loc);

        return tempVect;
    }
}
