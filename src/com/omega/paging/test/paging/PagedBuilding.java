package com.omega.paging.test.paging;

import com.omega.paging.entity.PagedObject;
import com.omega.paging.util.Vector3f;
import com.jme3.scene.Spatial;

/**
 *
 * @author Kyu
 */
public class PagedBuilding extends PagedObject<Spatial> {

    public PagedBuilding(Spatial object) {
        super(object);
    }

    @Override
    public Vector3f getPosition() {
        float[] loc = null;
        object.getWorldTranslation().toArray(loc);
        tempVect.setArray(loc);

        return tempVect;
    }
}
